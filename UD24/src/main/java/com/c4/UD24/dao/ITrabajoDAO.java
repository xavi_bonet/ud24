package com.c4.UD24.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.c4.UD24.dto.Trabajo;

public interface ITrabajoDAO extends JpaRepository<Trabajo, Long>{

	//Listar trabajo por campo nombre
	public List<Trabajo> findByNombre(String nombre);
	
}
