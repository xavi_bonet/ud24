package com.c4.UD24.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD24.dto.Empleado;
import com.c4.UD24.service.EmpleadoServiceImpl;



@RestController
@RequestMapping("/api")
public class EmpleadoController {

	@Autowired
	EmpleadoServiceImpl empleadoServiceImpl;
	
	@GetMapping("/empleados")
	public List<Empleado> listarEmpleados(){
		return empleadoServiceImpl.listarEmpleados();
	}
	
	//listar Empleados por campo nombre
	@GetMapping("/empleados/nombre/{nombre}")
	public List<Empleado> listarEmpleadoNombre(@PathVariable(name="nombre") String nombre) {
	    return empleadoServiceImpl.listarEmpleadoNomnbre(nombre);
	}
	
	//listar Empleados por campo id_trabajo
	@GetMapping("/empleados/idTrabajo/{id_trabajo}")
	public List<Empleado> listarEmpleadoIdTrabajo(@PathVariable(name="id_trabajo") long id_trabajo) {
	    return empleadoServiceImpl.listarEmpleadoIdTrabajo(id_trabajo);
	}
	
	@PostMapping("/empleados")
	public Empleado salvarEmpleado(@RequestBody Empleado empleado) {
		return empleadoServiceImpl.guardarEmpleado(empleado);
	}
	
	@GetMapping("/empleados/{id_empleado}")
	public Empleado empleadoXID(@PathVariable(name="id_empleado") Long id_empleado) {
		Empleado empleado_xid= new Empleado();
		
		empleado_xid=empleadoServiceImpl.empleadoXID(id_empleado);
		
		System.out.println("Empleado XID: "+empleado_xid);
		return empleado_xid;
	}
	
	@PutMapping("/empleados/{id_empleado}")
	public Empleado actualizarEmpleado(@PathVariable(name="id_empleado")Long id_empleado,@RequestBody Empleado empleado) {
		Empleado empleado_seleccionado= new Empleado();
		Empleado cliente_actualizado= new Empleado();
		
		empleado_seleccionado= empleadoServiceImpl.empleadoXID(id_empleado);
		empleado_seleccionado.setNombre(empleado.getNombre());
		empleado_seleccionado.setId_trabajo(empleado.getId_trabajo());
		
		empleado_seleccionado = empleadoServiceImpl.actualizarEmpleado(empleado_seleccionado);
		
		System.out.println("El empleado actualizado es: "+ empleado_seleccionado);
		return cliente_actualizado;
	}
	
	@DeleteMapping("/empleados/{id_empleado}")
	public void eleiminarEmpleado(@PathVariable(name="id_empleado")Long id_empleado) {
		empleadoServiceImpl.eliminarEmpleado(id_empleado);
	}
	
}
