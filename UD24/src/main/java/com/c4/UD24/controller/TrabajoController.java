package com.c4.UD24.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD24.dto.Trabajo;
import com.c4.UD24.service.TrabajoServiceImpl;

@RestController
@RequestMapping("/api")
public class TrabajoController {

	@Autowired
	TrabajoServiceImpl TrabajoServiceImpl;
	
	@GetMapping("/trabajos")
	public List<Trabajo> listarTrabajos(){
		return TrabajoServiceImpl.listarTrabajos();
	}
	
	//listar Trabajos por campo nombre
	@GetMapping("/trabajos/nombre/{nombre}")
	public List<Trabajo> listarTrabajoNombre(@PathVariable(name="nombre") String nombre) {
	    return TrabajoServiceImpl.listarTrabajoNomnbre(nombre);
	}
	
	@PostMapping("/trabajos")
	public Trabajo salvarTrabajo(@RequestBody Trabajo trabajo) {
		return TrabajoServiceImpl.guardarTrabajo(trabajo);
	}
	
	@GetMapping("/trabajos/{id_trabajo}")
	public Trabajo trabajoXID(@PathVariable(name="id_trabajo") Long id_trabajo) {
		Trabajo trabajo_xid= new Trabajo();
		trabajo_xid=TrabajoServiceImpl.trabajoXID(id_trabajo);
		System.out.println("Trabajo XID: "+trabajo_xid);
		return trabajo_xid;
	}
	
	@PutMapping("/trabajos/{id_trabajo}")
	public Trabajo actualizarTrabajo(@PathVariable(name="id_trabajo")Long id_trabajo,@RequestBody Trabajo trabajo) {
		Trabajo trabajo_seleccionado= new Trabajo();
		Trabajo trabajo_actualizado= new Trabajo();
		
		trabajo_seleccionado= TrabajoServiceImpl.trabajoXID(id_trabajo);
		trabajo_seleccionado.setNombre(trabajo.getNombre());
		trabajo_seleccionado.setId_trabajo(trabajo.getId_trabajo());
		
		trabajo_seleccionado = TrabajoServiceImpl.actualizarTrabajo(trabajo_seleccionado);
		
		System.out.println("El trabajo actualizado es: "+ trabajo_seleccionado);
		return trabajo_actualizado;
	}
	
	@DeleteMapping("/trabajos/{id_trabajo}")
	public void eleiminarTrabajo(@PathVariable(name="id_trabajo")Long id_trabajo) {
		TrabajoServiceImpl.eliminarTrabajo(id_trabajo);
	}
	
}
