package com.c4.UD24.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.c4.UD24.dao.IEmpleadoDAO;
import com.c4.UD24.dto.Empleado;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService {

	@Autowired
	IEmpleadoDAO iEmpleadoDAO;
	
	@Override
	public List<Empleado> listarEmpleados() {
		return iEmpleadoDAO.findAll();
	}

	@Override
	public Empleado guardarEmpleado(Empleado empleado) {
		return iEmpleadoDAO.save(empleado);
	}

	@Override
	public Empleado empleadoXID(Long id_empleado) {
		return iEmpleadoDAO.findById(id_empleado).get();
	}

	@Override
	public Empleado actualizarEmpleado(Empleado empleado) {
		return iEmpleadoDAO.save(empleado);
	}

	@Override
	public void eliminarEmpleado(Long id_empleado) {
		iEmpleadoDAO.deleteById(id_empleado);
	}
	
	@Override
	public List<Empleado> listarEmpleadoNomnbre(String nombre) {
		return iEmpleadoDAO.findByNombre(nombre);
	}
	
	@Override
	public List<Empleado> listarEmpleadoIdTrabajo(long id_trabajo) {
		return iEmpleadoDAO.findByIdTrabajo(id_trabajo);
	}

}
