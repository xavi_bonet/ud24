package com.c4.UD24.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.c4.UD24.dao.ITrabajoDAO;
import com.c4.UD24.dto.Trabajo;

@Service
public class TrabajoServiceImpl implements ITrabajoService{

	@Autowired
	ITrabajoDAO iTrabajoDAO;
	
	@Override
	public List<Trabajo> listarTrabajos() {
		return iTrabajoDAO.findAll();
	}

	@Override
	public Trabajo guardarTrabajo(Trabajo trabajo) {
		return iTrabajoDAO.save(trabajo);
	}

	@Override
	public Trabajo trabajoXID(Long id_trabajo) {
		return iTrabajoDAO.findById(id_trabajo).get();
	}

	@Override
	public Trabajo actualizarTrabajo(Trabajo trabajo) {
		return iTrabajoDAO.save(trabajo);
	}

	@Override
	public void eliminarTrabajo(Long id_trabajo) {
		iTrabajoDAO.deleteById(id_trabajo);
	}
	
	@Override
	public List<Trabajo> listarTrabajoNomnbre(String nombre) {
		return iTrabajoDAO.findByNombre(nombre);
	}

}
