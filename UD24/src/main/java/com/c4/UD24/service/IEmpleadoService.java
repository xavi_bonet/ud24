package com.c4.UD24.service;

import java.util.List;

import com.c4.UD24.dto.Empleado;

public interface IEmpleadoService {

	//Metodos del CRUD
	public List<Empleado> listarEmpleados(); //Listar All 
	
	public Empleado guardarEmpleado(Empleado empleado);	//Guarda un empleado CREATE
	
	public Empleado empleadoXID(Long id_empleado); //Leer datos de un empleado READ
	
	public List<Empleado> listarEmpleadoNomnbre(String nombre);//Listar empleado por campo nombre
	
	public Empleado actualizarEmpleado(Empleado empleado); //Actualiza datos del empleado UPDATE
	
	public void eliminarEmpleado(Long id_empleado); // Elimina el empleado DELETE

	public List<Empleado> listarEmpleadoIdTrabajo(long id_trabajo); //Listar empleado por campo id_trabajo
}
