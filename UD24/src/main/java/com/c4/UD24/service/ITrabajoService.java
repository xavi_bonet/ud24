package com.c4.UD24.service;

import java.util.List;

import com.c4.UD24.dto.Trabajo;

public interface ITrabajoService {

	//Metodos del CRUD
	public List<Trabajo> listarTrabajos(); //Listar All 
	
	public Trabajo guardarTrabajo(Trabajo trabajo);	//Guarda un Trabajo CREATE
	
	public Trabajo trabajoXID(Long id_trabajo); //Leer datos de un Trabajo READ
	
	public List<Trabajo> listarTrabajoNomnbre(String nombre);//Listar Trabajo por campo nombre
	
	public Trabajo actualizarTrabajo(Trabajo trabajo); //Actualiza datos del Trabajo UPDATE
	
	public void eliminarTrabajo(Long id_trabajo);// Elimina el Trabajo DELETE
	
}
