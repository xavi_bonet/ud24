package com.c4.UD24.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Empleados")
public class Empleado {

	// Atributos de entidad Empleado
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_empleado;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "id_trabajo")
	private Long idTrabajo;

	
	// Constructores
	
	public Empleado() {
		
	}

	public Empleado(Long id_empleado, String nombre, Long id_trabajo) {
		this.id_empleado = id_empleado;
		this.nombre = nombre;
		this.idTrabajo = id_trabajo;
	}

	// Getters y Setters
	
	public Long getId_empleado() {
		return id_empleado;
	}
	
	public void setId_empleado(Long id_empleado) {
		this.id_empleado = id_empleado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getId_trabajo() {
		return idTrabajo;
	}

	public void setId_trabajo(Long id_trabajo) {
		this.idTrabajo = id_trabajo;
	}
	
	// Metodo impresion de datos por consola
	
	@Override
	public String toString() {
		return "Empleado [id_empleado=" + id_empleado + ", nombre=" + nombre + ", id_trabajo=" + idTrabajo + "]";
	}
	
}
