package com.c4.UD24.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Trabajos") // en caso que la tabala sea diferente
public class Trabajo {
	
	// Atributos de entidad Trabajo
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_trabajo;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "salario")
	private int salario;

	// Constructores
	
	public Trabajo() {
		
	}
	
	public Trabajo(Long id_trabajo, String nombre, int salario) {
		this.id_trabajo = id_trabajo;
		this.nombre = nombre;
		this.salario = salario;
	}
	
	// Getters y Setters
	
	public Long getId_trabajo() {
		return id_trabajo;
	}

	public void setId_trabajo(Long id_trabajo) {
		this.id_trabajo = id_trabajo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getSalario() {
		return salario;
	}

	public void setSalario(int salario) {
		this.salario = salario;
	}
	
	// Metodo impresion de datos por consola

	@Override
	public String toString() {
		return "Trabajo [id_trabajo=" + id_trabajo + ", nombre=" + nombre + ", salario=" + salario + "]";
	}
	
}
