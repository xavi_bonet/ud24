DROP table IF EXISTS Empleados;
DROP table IF EXISTS Trabajos;

create table Trabajos(
    id_trabajo int auto_increment,
    nombre varchar(250),
    salario int,
    PRIMARY KEY (id_trabajo)
);

create table Empleados(
    id_empleado int auto_increment,
    nombre varchar(250),
    id_trabajo int,
    PRIMARY KEY (id_empleado),
    FOREIGN KEY (id_trabajo) REFERENCES Trabajos(id_trabajo)
);


insert into Trabajos (nombre, salario)values('Jefe',2200);
insert into Trabajos (nombre, salario)values('Encargado',1900);
insert into Trabajos (nombre, salario)values('Programador',1100);
insert into Trabajos (nombre, salario)values('Administrador',1200);
insert into Trabajos (nombre, salario)values('Voluntario',300);

insert into Empleados(nombre, id_trabajo) values('Xavi', 2);